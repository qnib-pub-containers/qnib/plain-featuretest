# syntax=docker/dockerfile:1
ARG IMG_TAG
FROM alpine:$IMG_TAG
ARG TARGETPLATFORM
ARG BUILDPLATFORM
COPY bin/entry.sh /usr/bin/entry.sh
CMD ["/usr/bin/entry.sh"]
ENV SKIP_ENTRYPOINTS=true