#!/bin/ash

DPUSH="${DPUSH:=false}"
IMAGE_URI="${IMAGE_URI:=qnib/plain-featuretest}"
IMAGE_TAG="${IMAGE_TAG:=main}"
: ${ARCH:="amd64;platform,cpu:broadwell,cpu:haswell,cpu:cascadelake,cpu:skylake_avx512,cpu:icelake,cpu:zen,cpu:zen2,cpu:zen3|arm64;platform,graviton2,graviton3|arm/v6;platform|arm/v7;platform|riscv64;platform"}

for ELE in $( echo $ARCH |tr "|" " ");do
  A=$(echo ${ELE} |awk -F";" '{print $1}')
  TARGETS=$(echo ${ELE} |awk -F";" '{print $2}')
  if [[ -z "${TARGETS}" ]];then
    TARGETS="platform"
  fi
  for T in $(echo ${TARGETS} |tr ',' ' ');do
    IMG_NAME=$IMAGE_URI:$IMAGE_TAG-$(echo ${A} |tr '/' '_')-$(echo ${T}|tr ':' '_')
    if [[ "${T}" == "platform" ]];then
      IMG_NAME=$IMAGE_URI:$IMAGE_TAG-$(echo ${A} |tr '/' '_')
    else
      TAR=$(echo ${T} |awk -F':' '{print $1}')
      if [[ "${TAR}" == "cpu" ]];then
        MA=$(echo ${T} |awk -F':' '{print $2}')
      else 
        GENERIC=$(echo ${T} |awk -F':' '{print $2}')
      fi
    fi
    
    if [[ "${T}" == "platform" ]];then
      T=$(echo ${A}|tr '/' '_')
    fi
    SRC_TAG=$(echo ${CI_COMMIT_TAG} |sed -e 's/-.*$//')
    set -x
    #docker pull --platform linux/${A} alpine:$SRC_TAG
    if [[ $? -ne 0 ]];then
      echo "Failed to pull alpine:$SRC_TAG for platform linux/${A}"
      continue
    fi
    case "${TAR}" in
    cpu)
      docker buildx build --pull -f Dockerfile.microarch \
      --platform linux/${A} --build-arg=IMG_TAG=$SRC_TAG \
      --label=ORG_SUPERCONTAINERS_HARDWARE_CPU_TARGET=microarch --label=ORG_SUPERCONTAINERS_HARDWARE_CPU_TARGET_MICROARCH=${MA} \
      --build-arg=MICROARCH=${MA} \
      --load -t $IMG_NAME .
      ;;
    generic)
      echo ">> This container is optimized for the generic target '${ORG_SUPERCONTAINERS_HARDWARE_CPU_TARGET_GENERIC}'"
      ;;
    *)
      echo ">> '${T}' : This container is not optimized for a specific cpu target"
      ;;
    esac

    if [[ ${DPUSH} == "true" ]];then
      docker push $IMG_NAME
    fi
    sleep .2
  done
done
