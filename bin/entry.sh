#!/bin/ash

case ${ORG_SUPERCONTAINERS_HARDWARE_CPU_TARGET} in
  microarch)
    echo ">> This container is optimized for the microarchitecture '${ORG_SUPERCONTAINERS_HARDWARE_CPU_TARGET_MICROARCH}'"
    ;;
  generic)
    echo ">> This container is optimized for the generic target '${ORG_SUPERCONTAINERS_HARDWARE_CPU_TARGET_GENERIC}'"
    ;;
  *)
    echo ">> This container is not optimized for a specific cpu target"
    ;;
esac
